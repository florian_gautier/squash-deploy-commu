# squash-deploy-commu

The goal si to deploy "simply", using docker compose, the following stack

- Squash TM with its Postgres database
- Squash Orchestrator
- Traefik for routing purposes (OPT.)
- Dozzle (OPT.) to diplay logs

The compose file is tested with the environment

![Deploiement test setup](static/images/deploiement.png)

## Pre-requisites

- git
- docker (Some host and a cli)
- docker-compose
- "Squash Autom" Squash TM plugins  available at [squashtest.com](https://squashtest.com).

On your windows/linux laptop if you want to register an agent

- Python >= 3.8
- The libs your tests needs (in my case it was robotframework and seleniumrobotframework)

## Workaround concerning Squash TM plugins volume mount

As of today the plugin directory of the Squash TM image is already populated with plugins. Mounting a volume on will wipeout the plugins dir existing content. Hence one must first retrieve this content in order to the complet it with new plugins. To do so a simple way is as follows :

1. launch a dummy squash tm container

    ``` bash
    docker run --name="squashtm-to-dump" squashtest/squash-tm:2.1.1
    ```

2. retreive the /opt/squash-tm/plugins content. Assuming your shell current directory is the one of this README file

     ``` bash
    docker cp squashtm-to-dump:/opt/squash-tm/plugins/  .
    ```

Then add in the plugins directrory the decrompressed archives extracted jars  you dowloaded as a pre-requisite

This "plugins" directory is ready to be mounted as a volume at /opt/squash-tm/volume in the Squash TM container.

## How to use the compose file

The compose file assume a "plugins" directory located at ./plugins relative to the location of the README file.

To launch it just use :

``` bash
docker-compose up
```

After a few seconds/minutes (depending on the app) you should be able to access

- Traefik dashboard at <http://localhost:8081>
- Squash TM at  <http://localhost/squash>
- The orchestrator on various routes including <http://localhost/workflows>
- Dozzle log provider at <http://localhost/logs>

## Squash TM / Orchestrator link configuration

Follow the recipe [here](https://squashtest.gitlab.io/doc/squash-autom-devops-doc-fr)
, keeping in mind that the public IP adresses should be de one of the WSL2 debian machine...)

## Useful links

- [Squashtest download section](https://squashtest.com)
- [The Orchetrator documentation](https://opentestfactory.gitlab.io/orchestrator/)
- [Squash AUTOM Documenation](https://squashtest.gitlab.io/doc/squash-autom-devops-doc-fr)
